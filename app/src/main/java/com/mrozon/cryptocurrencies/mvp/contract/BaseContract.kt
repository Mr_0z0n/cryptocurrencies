package com.mrozon.cryptocurrencies.mvp.contract

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

class BaseContract {

    interface View

    abstract class Presenter<V: View>{
        private val subcriptions = CompositeDisposable()
        protected lateinit var view: V

        fun subscribe(subcription: Disposable){
            subcriptions.add(subcription)
        }

        fun unsubscribe(){
            subcriptions.clear()
        }

        fun attach(view:  V){
            this.view = view
        }

        fun detach(){
            unsubscribe()
        }
    }
}