package com.mrozon.cryptocurrencies.di

import com.mrozon.cryptocurrencies.activities.ChartActivity
import com.mrozon.cryptocurrencies.activities.MainActivity
import com.mrozon.cryptocurrencies.chart.LatestChart
import com.mrozon.cryptocurrencies.fragments.CurrenciesListFragment
import com.mrozon.cryptocurrencies.mvp.presenter.CurrenciesPresenter
import com.mrozon.cryptocurrencies.mvp.presenter.LatestChartPresenter
import dagger.Component
import javax.inject.Singleton

@Component(modules = arrayOf(AppModule::class, RestModule::class, MvpModule::class, ChartModule::class))
@Singleton
interface AppComponent {
    fun inject(mainActivity: MainActivity)
    fun inject(presenter: CurrenciesPresenter)
    fun inject(presenter: LatestChartPresenter)
    fun inject(fragment: CurrenciesListFragment)
    fun inject(chart: LatestChart)
    fun inject(chartActivity: ChartActivity)
}