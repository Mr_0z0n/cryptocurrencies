package com.mrozon.cryptocurrencies.di

import android.content.Context
import com.mrozon.cryptocurrencies.mvp.presenter.CurrenciesPresenter
import com.mrozon.cryptocurrencies.mvp.presenter.LatestChartPresenter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val app: App) {

    @Provides
    @Singleton
    fun provideContext(): Context = app

}