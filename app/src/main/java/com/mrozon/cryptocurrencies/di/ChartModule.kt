package com.mrozon.cryptocurrencies.di

import com.mrozon.cryptocurrencies.YearValueFormatter
import com.mrozon.cryptocurrencies.chart.LatestChart
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ChartModule {

    @Provides
    @Singleton
    fun provideLatestChart() = LatestChart()


    @Provides
    @Singleton
    fun provideYearFormatter() = YearValueFormatter()
}