package com.mrozon.cryptocurrencies.chart

import android.content.Context
import android.widget.TextView
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF
import com.mrozon.cryptocurrencies.R
import com.mrozon.cryptocurrencies.dateToString
import kotlinx.android.synthetic.main.custom_marker_view.view.*

class MyMarkerView (context: Context, layoutResurce: Int) : MarkerView(context, layoutResurce) {
    private val tvContent: TextView

    init {
        tvContent = findViewById(R.id.tvContent)
    }

    override fun refreshContent(e: Entry?, highlight: Highlight?) {
        tvContent.text = e?.y.toString() + "\n" +e?.x!!.dateToString("MMM dd, yyyy")
        super.refreshContent(e, highlight)
    }

    override fun getOffset(): MPPointF {
        return MPPointF((-(width / 2)).toFloat(), (-height).toFloat())
    }
}